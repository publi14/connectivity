package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"os/user"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/go-ping/ping"
)

// Fallback IP TO TRACERT WHEN CONFIG FILE IS INNACCESSIBLE
var ipToTracert string = "8.8.8.8"

// Fallback IPs TO PING WHEN CONFIG FILE IS INNACCESSIBLE
var pingIps = []string{"1.1.1.1"}

// Fallback DNS SERVERS WHEN CONFIG FILE IS INNACCESSIBLE
var dnsServerIps = []string{"8.8.8.8"}

// Fallback DNS DOMAINS TO CHECK WHEN CONFIG FILE IS INNACCESSIBLE
var dnsDomains = []string{"linkedin.com"}

// Fallback ENDPOINTS FOR HTTP GET WHEN CONFIG FILE IS INNACCESSIBLE
var httpGetEndpoints = []string{"https://stackoverflow.com/"}

var debugLogPath, username string

func pushToRemote(postBody []byte) {
	// POSTING DATA TO AN ENDPOINT
	responseBody := bytes.NewBuffer(postBody)
	fmt.Println("Initiating POST")
	// HTTP POST
	resp, postErr := http.Post("Https endpoint to post results to", "application/json", responseBody)
	// Handle Error if any
	if postErr != nil {
		log.Fatalf("An Error Occured %v", postErr)
	}
	fmt.Println("POST successful")
	// Read the response body
	_, ioUtilErr := ioutil.ReadAll(resp.Body)
	if ioUtilErr != nil {
		log.Fatalln(ioUtilErr)
	}

	// CLOSE RESPONSE BODY BEFORE LEAVING THE FUNCTION
	defer func() {
		_ = resp.Body.Close()
	}()

}

// MAX SIZE OF APP DATA LOG FILE, WHEN EXCEEDED LOG IS CLEARED
// 1 MB
var maxLogSize int64 = 1048576

// Function to check log size
func checkLogSize(logpath string) {
	// CHECK IF FILE EXISTS BEFORE CHECKING ITS FILE SIZE
	if _, err := os.Stat(logpath); os.IsNotExist(err) {
		// FILE DOES NOT EXIST, SKIP CHECK AS THERE IS NO NEED OF CHECKING FILE SIZE
	} else {
		// FILE EXISTS, GET FILE SIZE
		fileSize, err := os.Stat(logpath)
		if err != nil {
			log.Println("Couldnt get file info")
			return
		}
		fileSizeInt := fileSize.Size()
		if fileSizeInt > maxLogSize {
			e := os.Remove(logpath)
			if e != nil {
				log.Printf("Unable to delete activity log log file %s", e)
			}
		}
	}
}

// Function to check DNS
func dnsChecker() string {
	nsLookupResultDictionary := make(map[string]map[string]string)
	for _, eachDomainName := range dnsDomains {
		nsLookupResultForEachDomain := make(map[string]string)
		for _, eachDNSServer := range dnsServerIps {
			fmt.Printf("Checking Domain:%s against DNS Server:%s\n", eachDomainName, eachDNSServer)
			cmd_instance := exec.Command("nslookup", eachDomainName, eachDNSServer)
			cmd_instance.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}
			RawNSLookupResults, nslookupErr := cmd_instance.Output()
			if nslookupErr != nil {
				fmt.Printf("NSLookup has failed : %s\n", nslookupErr)
				nsLookupResultForEachDomain[eachDNSServer] = "fail"
			} else {
				splittedListOfResults := strings.Split(string(RawNSLookupResults), "\n")
				if strings.Contains(splittedListOfResults[0], "DNS request timed out.") {
					log.Printf("DNS Lookup for domain:%s on server:%s has failed.\n", eachDomainName, eachDNSServer)
					nsLookupResultForEachDomain[eachDNSServer] = "fail"
				} else {
					fmt.Printf("DNS Lookup for domain:%s on server:%s successful.\n", eachDomainName, eachDNSServer)
					nsLookupResultForEachDomain[eachDNSServer] = "success"
				}
			}
		}
		// ADD THIS TO THE FINAL DICTIONARY
		nsLookupResultDictionary[eachDomainName] = nsLookupResultForEachDomain
	}
	jsonStringFromMap, _ := json.Marshal(nsLookupResultDictionary)
	return string(jsonStringFromMap)

}

// Function to write data to file
func writeToFile(pingResult string, traceResult string, httpGetResult string, dnsResults string, status string) string {

	dt := time.Now()
	result_data := fmt.Sprintf(`{"date": "%s",  "ping": %s , "tracert": %s, "httpGet": %s, "DNS Result": %s,"status": "%s"}`, dt.Format("2006-01-02 15:04:05"), pingResult, traceResult, httpGetResult, dnsResults, status)
	rawIn := json.RawMessage(result_data)
	dataBody, _ := rawIn.MarshalJSON()

	finalResultString := string(dataBody)

	// CHECK IF LOCAL ACTIVITY LOG FOLDER EXISTS,  IF FOLDER DOES NOT EXIST
	if _, err := os.Stat(fmt.Sprintf("C:/Users/%s/AppData/Local/ptr/activitylog", username)); os.IsNotExist(err) {

		// CREATE THE FOLDER
		fmt.Printf("username for activity is %s", username)
		err := os.Mkdir(fmt.Sprintf("C:/Users/%s/AppData/Local/ptr/activitylog", username), 0755)
		// IF THERE WAS AN ERROR CREATING THE LOG DIRECTORY
		if err != nil {
			log.Println("Local activity log folder creation has failed")

		} else {
			log.Println("Local activity log folder creation has succeeded")

			// CREATE THE NEW LOG FILE
			f, err := os.Create(fmt.Sprintf("C:/Users/%s/AppData/Local/ptr/activitylog/log.txt", username))
			// iF THERE WAS AN ERROR CREATING THE FILE
			if err != nil {
				log.Println(err)
			}
			// WRITE TO THE FILE
			written, err := f.WriteString(finalResultString)
			// IF THERE WAS AN ERROR WRITING TO THE FILE
			if err != nil {
				log.Println(fmt.Sprintf("Unable to write to log to the newly created local activity log file : %s \n", err))
				f.Close()
			}
			err = f.Close()
			// IF THERE WAS AN ERROR CLOSING THE OPEN FILE
			log.Println(written)
			if err != nil {
				log.Println(fmt.Sprintf("Unable to close local activity log file : %s", err))
			}
		}
	} else {
		log.Println("Local activity log Folder exists")
		// THE PATH WHERE THE RESULTS OF THE PINGS, TRACE ARE STORED
		activityLogPath := fmt.Sprintf("C:/Users/%s/AppData/Local/ptr/activitylog/log.txt", username)
		// CHECK IF LOG SIZE IS WITHIN ALLOWED SIZE
		checkLogSize(activityLogPath)

		// RESUME NORMAL OPERATIONS AFTER CHECKING FILE
		f, err := os.OpenFile(activityLogPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

		// UNABLE TO OPEN OR CREATE LOG FILE
		if err != nil {
			log.Println(fmt.Sprintf("Local activity log file opening has failed : %s", err))
		} else {
			// IF THERE WERE NO ERRORS WHEN OPENING THE FILE, GO AHEAD AND WRITE TO IT
			_, err = fmt.Fprintln(f, finalResultString)
			// IF THERE WAS AN ERROR WITH APPENDING TO THE FILE
			if err != nil {
				log.Println(fmt.Sprintf("Problem writing to local activity log file :  %s", err))
				f.Close()
			} else {
				fmt.Print("Local activity log file save successful \n")
			}
		}
	}

	return finalResultString

}

// Function to perform Get on HTTP endpoints
func getHttpEndpoints() string {
	// fmt.Printf("Endie is %s of length %v", httpGetEndpoints, len(httpGetEndpoints))
	httpResultList := make([]map[string]string, 0)
	for _, eachEndpoint := range httpGetEndpoints {
		// MAP BELOW STORES RESULT OF EACH GET REQUEST
		httpGetDictionary := make(map[string]string)

		start := time.Now()

		// DO HTTP GET FOR THE ENDPOINTS
		_, httpErr := http.Get(eachEndpoint)
		//  IF HTTP WASN'T SUCCESSFUL
		if httpErr != nil {
			httpGetDictionary[eachEndpoint] = "fail"
		} else {
			httpGetDictionary[eachEndpoint] = "success"
		}
		elapsed := time.Since(start).Seconds()
		httpGetDictionary["time"] = fmt.Sprintf("%.4f", elapsed)

		httpResultList = append(httpResultList, httpGetDictionary)

		fmt.Printf("http.Get to %s took %v seconds \n", eachEndpoint, elapsed)
	}
	jsonStringFromMap, _ := json.Marshal(httpResultList)
	return fmt.Sprintf("%s", jsonStringFromMap)

}

// HOW LONG SHOULD PROGRAM SLEEP
var secondsToSleep int = 300

// Function to perform traceroute
func tracer() (string, bool) {
	fmt.Printf("Tracing %s........\n", ipToTracert)
	var traceUnsuccessful = false
	// EXECUTE TRACERT
	cmd_instance := exec.Command("tracert", "/h", "8", ipToTracert)
	cmd_instance.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}
	RawTraceResults, tracertErr := cmd_instance.Output()
	if tracertErr != nil {
		log.Printf("Tracert has failed : %s", tracertErr)
		return "failed", false
	} else {
		splittedListOfResults := strings.Split(string(RawTraceResults), "\n")

		// LINE ONE i.e.
		// Tracing route to subdomain.domain.co.ke [xxx.xxx.xxx.xxx]

		// LINE 2 i.e.
		// over a maximum of 8 hops:

		traceResultsRaw_ := splittedListOfResults[4:]
		// {"  1     6 ms     8 ms     5 ms  xxx.xxx.xxx.xxx \r", "  2     8 ms    13 ms     8 ms  subdomain.domain.co.ke [xxx.xxx.xxx.xxx] \r"}
		traceResultsRaw := traceResultsRaw_[:len(traceResultsRaw_)-3]

		// var traceResults [][]string
		var traceResults []string
		for count, eachHost := range traceResultsRaw {
			// REMOVES SPACES FROM SLICE
			singleTraceResult := strings.Fields(eachHost)

			// THIS MEANS THERE'S A REQUEST TIMEOUT WITH THE FIRST TRACERT REQUEST, WHICH IS MAINLY ASSUMED TO BE UNREACHABLE SERVER
			if strings.Contains(eachHost, "Request timed out.") && count == 0 {
				log.Println("Server tracert unsuccessful")
				traceUnsuccessful = true
				break
			} else {
				if len(singleTraceResult) <= 7 {
					break
				}
			}

			// MILLISECONDS FOR THE FIRST PACKET
			firstPacketDuration := singleTraceResult[1]
			if strings.Contains(firstPacketDuration, "<") {
				// TO AVOID STRING REPRESENTATION ISSUE
				firstPacketDuration = "0"
			}
			// MILLISECONDS FOR THE SECOND PACKET
			secondPacketDuration := singleTraceResult[3]
			if strings.Contains(secondPacketDuration, "<") {
				// TO AVOID STRING REPRESENTATION ISSUE
				secondPacketDuration = "0"
			}
			// MILLISECONDS FOR THE THIRD PACKET
			thirdPacketDuration := singleTraceResult[5]
			if strings.Contains(thirdPacketDuration, "<") {
				// TO AVOID STRING REPRESENTATION ISSUE
				thirdPacketDuration = "0"
			}
			domainNameOrIP := singleTraceResult[7]
			// fmt.Printf("first packet is %#v, second %#v, third %#v \n", firstPacketDuration, secondPacketDuration, thirdPacketDuration)
			// var eachHostData []string
			var eachHostData string
			// if len(singleTraceResult) >= 9 {
			if len(singleTraceResult) >= 9 {
				charactersToReplace := strings.NewReplacer("[", "", "]", "")
				ipOfTheFoundDomain := charactersToReplace.Replace(singleTraceResult[8])
				// {"1", "8", "ms", "13", "ms", "8", "ms", "domain.co.ke", "[xxx.xxx.xxx.xxx]"}
				//eachHostData = []string{firstPacketDuration, secondPacketDuration, thirdPacketDuration, domainNameOrIP, ipOfTheFoundDomain}
				eachHostData = fmt.Sprintf("%s %s %s %s %s", firstPacketDuration, secondPacketDuration, thirdPacketDuration, domainNameOrIP, ipOfTheFoundDomain)
			} else {
				// {"1", "6", "ms", "8", "ms", "5", "ms", "xxx.xxx.xxx.xxx"}
				// {fmt.Sprintf( "ms", "8","%s %s %s %s %s",  "ms", "5", "ms", "xxx.xxx.xxx.xxx")
				// eachHostData = []string{firstPacketDuration, secondPacketDuration, thirdPacketDuration, domainNameOrIP}
				eachHostData = fmt.Sprintf("%s %s %s %s", firstPacketDuration, secondPacketDuration, thirdPacketDuration, domainNameOrIP)
			}
			fmt.Printf("Each host data is %s", eachHostData)

			// traceResults = append(traceResults, eachHostData)
			traceResults = append(traceResults, eachHostData)
		}

		// IF TRACE WAS A SUCCESS
		if !traceUnsuccessful {
			fmt.Println("Trace successful")
			jsonTraceResults, _ := json.Marshal(traceResults)
			return string(jsonTraceResults), false
		} else {
			return "", true
		}
	}

}

// Function to perform Pings
func pinger() (string, bool) {
	var errorRate string
	// MAP BELOW STORES RESULT OF EACH PING IP
	pingDictionary := make(map[string]string)
	for _, eachIP := range pingIps {
		fmt.Printf("Pinging %s.........\n", eachIP)
		pinger, newPingerErr := ping.NewPinger(eachIP)
		pinger.SetPrivileged(true)
		if newPingerErr != nil {
			panic(newPingerErr)
		}

		// Listen for Ctrl-C.
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		go func() {
			for _ = range c {
				// LISTEN TO (ctrl+c) TO EXIT
				pinger.Stop()
			}
		}()

		pinger.Count = 3
		pinger.OnRecv = func(pkt *ping.Packet) {}

		pinger.OnFinish = func(stats *ping.Statistics) {
			s32 := strconv.FormatFloat(stats.PacketLoss, 'f', 1, 32)
			errorRate = fmt.Sprintf("%s", s32)
			pingDictionary[eachIP] = fmt.Sprintf("round-trip min/avg/max/stddev/errRate = %v/%v/%v/%v/%v", stats.MinRtt, stats.AvgRtt, stats.MaxRtt, stats.StdDevRtt, errorRate)
		}

		pingerErr := pinger.Run()
		// IF PINGER FAILS
		if pingerErr != nil {
			log.Printf("Ping for %s has failed : %s", eachIP, pingerErr)
			pingDictionary[eachIP] = "Failed"
		}
		fmt.Printf("Ping successful \n")
	}
	jsonStringFromMap, _ := json.Marshal(pingDictionary)
	return fmt.Sprintf("%s", jsonStringFromMap), false
}

func main() {

	// Listen for Ctrl-C.
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for _ = range c {
			// LISTEN TO (ctrl+c) TO EXIT
			os.Exit(0)
		}
	}()

	// GET USERNAME OF THE LOGGED IN USER
	currentUser, err := user.Current()
	if err != nil {
		log.Fatalf(err.Error())
	}

	username = (strings.Split(currentUser.Username, "\\"))[1]

	// LOCAL PROGRAM LOG LOCATION
	if _, err := os.Stat(fmt.Sprintf("C:/Users/%s/AppData/Local/ptr", username)); os.IsNotExist(err) {
		errorOnMkdir1 := os.Mkdir(fmt.Sprintf("C:/Users/%s/AppData/Local/ptr", username), 0755)
		errorOnMkdir2 := os.Mkdir(fmt.Sprintf("C:/Users/%s/AppData/Local/ptr/programlog", username), 0755)
		if errorOnMkdir1 != nil || errorOnMkdir2 != nil {
			log.Printf("Unable to create directory for storing program log. First if : %s :%s", errorOnMkdir1, errorOnMkdir2)
		}
	} else {
		if _, err := os.Stat(fmt.Sprintf("C:/Users/%s/AppData/Local/ptr/programlog", username)); os.IsNotExist(err) {
			errorOnMkdir2 := os.Mkdir(fmt.Sprintf("C:/Users/%s/AppData/Local/ptr/programlog", username), 0755)
			if errorOnMkdir2 != nil {
				log.Println("Unable to create directory for storing program log. Second if")
			}
		}
	}

	// THE PATH WHERE THE DEBUG LOGS ARE STORED
	debugLogPath = fmt.Sprintf("C:/Users/%s/AppData/Local/ptr/programlog/log.txt", username)

	// CHECK AND ACTION IF LOCAL LOG SIZE IS WITHIN ALLOWED SIZE
	checkLogSize(debugLogPath)

	file, err := os.OpenFile(debugLogPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Println(err)
	} else {
		// SET LOG TO WRITE TO FILE
		log.SetOutput(file)
	}

	// TRY LOADING THAT CONFIG FILE
	configFileLocation := "config.json"
	fileObj, err := ioutil.ReadFile(configFileLocation)
	if err != nil {
		log.Printf("Unable to load config file: %s", err)
	}
	//configFileContent := string(fileObj)

	// CONVERT TO MAP
	var objmaperr map[string]json.RawMessage
	marshallErr := json.Unmarshal(fileObj, &objmaperr)
	if err != nil {
		log.Printf("unable to parse Config json : %s", marshallErr)
	} else {
		pingIPsObject := objmaperr["pingIPs"]
		endpointsToGetObject := objmaperr["httpGetEndpoints"]
		dnsServerIpsObject := objmaperr["dnsServers"]
		dnsDomainsForChecking := objmaperr["domainsForDNS"]
		ipToTracert = string(objmaperr["tracertIP"])
		parsedsecondsToSleep, secondsToSleepConvError := strconv.Atoi(string(objmaperr["sleepSeconds"])[1 : len(string(objmaperr["sleepSeconds"]))-1])
		if secondsToSleepConvError == nil {
			secondsToSleep = parsedsecondsToSleep
			log.Println("Parsed time correctly")
		} else {
			log.Printf("Unable to parse sleepSeconds: %s", secondsToSleepConvError)
		}

		ipToTracert = ipToTracert[1 : len(ipToTracert)-1]
		errorWhenReadingIPsToPing := json.Unmarshal(pingIPsObject, &pingIps)
		errorWhenReadingEndpointsToGet := json.Unmarshal(endpointsToGetObject, &httpGetEndpoints)
		errorWhenReadingDomainsToDNSCheck := json.Unmarshal(dnsDomainsForChecking, &dnsDomains)
		errorWhenReadingDNSServersIp := json.Unmarshal(dnsServerIpsObject, &dnsServerIps)
		if errorWhenReadingIPsToPing != nil || errorWhenReadingEndpointsToGet != nil || errorWhenReadingDNSServersIp != nil || errorWhenReadingDomainsToDNSCheck != nil {
			log.Printf("Unable to marshall ips/endpoints to ping | %s | %s |%s |%s", errorWhenReadingIPsToPing, errorWhenReadingEndpointsToGet, errorWhenReadingDNSServersIp, errorWhenReadingDomainsToDNSCheck)
		}
	}

	for {

		// DO HTTP GET TO GOOGLE SERVER TO CHECK INTERNET CONNECTIVITY
		_, httpErr := http.Get("http://clients3.google.com/generate_204")
		//  IF HTTP WASN'T SUCCESSFUL MEANS NO INTERNET CONNECTION
		if httpErr != nil {
			log.Printf("No internet connection : %s", httpErr)
			writeToFile("Fail. No internet", "Fail. No internet", "Fail. No internet", "Fail. No internet", "fail")
			// PROGRAM SLEEPS FOR DEFINED TIME THEN RUNS AGAIN
			time.Sleep(time.Duration(secondsToSleep) * time.Second)
			// RESTART LOOP
			continue
		}

		// IF PING FAILS, DON'T RUN TRACERT, SKIP IT AND RETURN ERROR
		pingResult, pingError := pinger()
		if pingError {
			writeToFile("fail", "", "", "", "error")

			// PROGRAM SLEEPS FOR DEFINED TIME THEN RUNS AGAIN
			time.Sleep(time.Duration(secondsToSleep) * time.Second)
			// RESTART LOOP
			continue

		} else {
			// IF PING DOES NOT FAIL, RUN TRACERT
			traceResult, tracertError := tracer()
			// IF TRACERT DOES NOT RETURN AN ERROR, LOG THE RESULT
			if tracertError {
				// IF TRACERT RETURNS AN ERROR
				traceResult = "fail"
			}

			// DO HTTP GET CHECK
			httpGetStringResult := getHttpEndpoints()

			// DO DNS CHECK
			dnsResults := dnsChecker()
			result_string := writeToFile(pingResult, traceResult, httpGetStringResult, dnsResults, "success")
			pushToRemote([]byte(result_string))
		}

		// PROGRAM SLEEPS FOR DEFINED TIME THEN RUNS AGAIN
		fmt.Printf("Sleeping for %v seconds \n", secondsToSleep)
		fmt.Println("-------------------------------------")
		time.Sleep(time.Duration(secondsToSleep) * time.Second)
	}
}
