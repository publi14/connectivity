# **Connectivity**

## Purpose
To create a software that can measure network/connectivity availability and performance metrics by running on the host windows machine.

## Capability
The program performs ping, tracert, dns lookup and a HTTP Get.

## How it works
Config is passed to the program by using a config.json file. The config file is structured as below
```json
    {
    "pingIPs": ["a list of ip addresses to ping"],
    "httpGetEndpoints": ["a list of ip addresses to Get"],
    "dnsServers": ["A list of DNS server IP's to use for DNS resolution"],
    "domainsForDNS": ["a list of domain names to resolve"],
    "tracertIP": "IP/Domain name to perform tracert agains",
    "sleepSeconds": "time in seconds(String) the program will sleep for before running again."
}  
```
The program logs to two different files, one, the output of each run the other the program log data. Additionally the results can also be posted to an endpoint in ```JSON```

## Tested on
This has been tested on Windows 10.

## How to Run
Pull the code and run "```go run ptr.go```" or compile then execute.


*This is my first Go project* 🐱‍🏍🐱‍🏍

